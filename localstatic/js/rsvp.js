$(document).ready(function() {
   $('#rsvp_code').keyup(function(){
   	 if (this.value.length >= 4){
   	 	checkCode(this.value);
   	 }
   });
   
   var checkCode = function(val){
   	 $.get('/rsvp/checkcode/',
   	 		{
   	 	      code: val
   	 		},
   	 		function(response){
   	 			if (response.valueOf() != 'NOT FOUND'){
   	 				step2(response);
   	 			}
   	 		});
   }
   
   var step2 = function(response){
	$('#rsvp_code').attr('disabled', 'disabled');
	$('#rsvp_step1 span span').html('');
	$('#rsvp_step1 span.step_number').css('background-position', '0px 48px');
	$('#rsvp_step2').html(response.step2);
	$('#rsvp_step2').css('display', 'block');
	
	$('#rsvp_step3').html(response.step3);
	
	$('#step2_choices ul li span.choices a span.check').click(function(){
	   	$(this).css('background-position', '0px -24px');
	   	$(this).closest('li').find('.cross').css('background-position', '0px 24px');
	   	$(this).closest('li').find('.attendance-flag').removeClass('no').addClass('yes').html('- attending').css('visibility', 'visible');
	   	return false;
    });
    $('#step2_choices ul li span.choices a span.cross').click(function(){
	   	$(this).css('background-position', '0px -24px');
	   	$(this).closest('li').find('.check').css('background-position', '0px 24px');
	   	$(this).closest('li').find('.attendance-flag').removeClass('yes').addClass('no').html('- not attending').css('visibility', 'visible');
	   	return false;
    });
    $('#step2_choices ul li span.choices a span').click(function(){
    	if ($(this).hasClass('check'))
    		$(this).closest('li').find('input').val('yes');
    	if ($(this).hasClass('cross'))
    		$(this).closest('li').find('input').val('no');
    	if ($(this).closest('li').hasClass('plus-one') && $(this).hasClass('check')){
    		$('#plus-one-form').dialog({
    			modal: true,
    			resizable: false,
    			draggable: false,
    			title: "Please enter your guest's name",
    			buttons: {
    				'Ok': function(){
    					var first_name = $(this).find('.first_name').val();
    					var last_name = $(this).find('.last_name').val();
    					$('input[name=plus-one-first-name]').val(first_name);
    					$('input[name=plus-one-last-name]').val(last_name);
    					$('li.plus-one .guest_name').html(first_name + ' ' + last_name);
    					$('li.plus-one-meal .guest_name').html(first_name + ' ' + last_name);
    					$('li.plus-one').removeClass('plus-one');
    					$(this).dialog("close");
    					sync_step3();
    				}
    			}
    		});
    	}
    	var finished = true;
    	$('.attendance-flag').each(function(){
    		if (!$(this).hasClass('yes') && !$(this).hasClass('no')){
    			finished = false;
    		}
    	});
    	if (finished){
    		step3();
    	}
    });
   }
   
   var step3 = function(){
   	sync_step3();
	$('#rsvp_step2 span span').html('');
	$('#rsvp_step2 span.step_number').css('background-position', '0px 48px');
   	$('#rsvp_step3').css('display', 'block');
   	$('#step2_choices ul li span.choices a span').click(sync_step3);
   	
   	$('.meal-select').change(function(){
   		if ($('li:visible .meal-select option[value=0]:selected').length == 0){
   			$('#rsvp_step3 span span').html('');
			$('#rsvp_step3 span.step_number').css('background-position', '0px 48px');
			$('#rsvp-button').css('visibility', 'visible');
   		} else {
   			$('#rsvp_step3 span span').html('3');
			$('#rsvp_step3 span.step_number').css('background-position', '0px 0px');
			$('#rsvp-button').css('visibility', 'hidden');
   		}
   	});
   	
   	$('#rsvp-button').click(function(){
   		$('#rsvp_code').removeAttr('disabled');
   		$('#rsvp-form').submit();
   		return false;
   	});
   }
   
   var sync_step3 = function(){
   	$('.attendance-flag').each(function(){
   		var guest = $(this).closest('li').attr('class');
   		var going = $(this).hasClass('yes');
   		var step3_li = $('#rsvp_step3').find('.' + guest);
   		if (going){
   			step3_li.css('display', 'block');
   		} else {
   			step3_li.css('display', 'none');
   		}
   	});
   }
});