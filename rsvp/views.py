from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.shortcuts import render_to_response
from django.template import RequestContext
from wedding.rsvp.models import *
import json

def rsvp(request):
    if request.method == 'POST':
        invitation = Invitation.objects.get(code=request.POST['rsvp_code'])
        for guest in invitation.guest_set.all():
            guest_rsvp_key = "guest-%d-rsvp" % guest.id
            guest_meal_key = "guest-%d-meal" % guest.id
            if request.POST[guest_rsvp_key] == "yes":
                guest.is_coming = True
                guest.meal = request.POST[guest_meal_key]
                if guest.check_if_plus_one:
                    guest.first_name = request.POST['plus-one-first-name']
                    guest.last_name = request.POST['plus-one-last-name']
            else:
                guest.is_coming = False
            guest.save()
        request.session['rsvp_code'] = invitation.code
        invitation.is_complete = True
        invitation.dietary_needs = request.POST['dietary-needs']
        invitation.save()
        return HttpResponseRedirect("/rsvp/thanks/")
    return render_to_response("rsvp.html",
                              {},
                              RequestContext(request))

def thanks(request):
    if not 'rsvp_code' in request.session:
        return HttpResponseNotFound()
    return render_to_response("thanks.html",
                              {"invitation": Invitation.objects.get(code=request.session['rsvp_code'])},
                              RequestContext(request))

def checkcode(request):
    code = request.GET['code']
    try:
        invitation = Invitation.objects.get(code=code)
    except:
        return HttpResponse("NOT FOUND")
    response = {}
    response['step2'] = render_to_response("step2.html",
                                           {'invitation': invitation,},
                                           RequestContext(request)).content
    response['step3'] = render_to_response("step3.html",
                                           {'invitation': invitation,
                                            'meals': MEAL_CHOICES},
                                           RequestContext(request)).content
    return HttpResponse(json.dumps(response), mimetype='application/json')