from django.contrib import admin
from models import Invitation, Guest

class GuestInline(admin.StackedInline):
    model = Guest
    extra = 1

class InvitationAdmin(admin.ModelAdmin):
    inlines = [GuestInline,]
admin.site.register(Invitation, InvitationAdmin)