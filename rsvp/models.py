from django.db import models
import random

MEAL_CHOICES = (('Chicken', 'Quarter Roast Chicken'),
                ('Hip of Beef', 'Hip of Beef'),
                ('Salmon', 'Baked Atlantic Salmon'),
                ('Vegetable Lasagna', 'Vegetable Lasagna'))

class Invitation(models.Model):
    code = models.CharField(max_length=4, blank=True, editable=False)
    dietary_needs = models.TextField(blank=True)
    is_complete = models.BooleanField(default=False)
    
    def save(self, *args, **kwargs):
        if not self.code:
            self.code=Invitation.generate_code()
        super(Invitation, self).save(*args, **kwargs)
        
    def __unicode__(self):
        result = "Guests: "
        
        result += ", ".join([unicode(guest) for guest in self.guest_set.all()])
        result += " - Code: %s" % self.code
        return result
        
        
    @classmethod
    def generate_code(cls):
        alphabet = "abcdefghjkmnpqrstuvwxyz23456789"
        code = ""
        for i in range(0,4):
            code += random.choice(alphabet)
        return code

class Guest(models.Model):
    invitation = models.ForeignKey('Invitation', null=False)
    first_name = models.CharField(max_length=100, blank=True, null=False, help_text='Leave blank if unknown')
    last_name = models.CharField(max_length=100, blank=True, null=False, help_text='Leave blank for unknown')
    is_coming = models.NullBooleanField(default=None)
    meal = models.CharField(max_length=100, blank=True, null=False, choices=MEAL_CHOICES)
    check_if_plus_one = models.BooleanField(default=False)
    
    def is_blank(self):
        return not (self.first_name and self.last_name)
    
    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)